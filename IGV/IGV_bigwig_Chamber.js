

$(document).ready(function() {
    createModal()
    fillDaFeatures()
    fillTracks()
    fillFeatures()
    initDefaultTracks()

    $('#createIGVBButton').click(function(){
        collectTracksAndLaunchIGV()
    })
})


function createModal() {

    modals = [
        "specificSelectionModal",
        "trackSelectionModal",
        "featuresSelectionModal"
    ]


    activateModal(modals[0])
    activateModal(modals[1])
    activateModal(modals[2])

}


function activateModal(modalName) {
    console.log(modalName)
    var modal = document.getElementById(modalName);

    // Get the button that opens the modal
    var btn = document.getElementById(modalName + "Button");

    // Get the <span> element that closes the modal
    var span = document.getElementById(modalName + "Close");;

    // Get the <span> element that closes the modal
    var close = document.getElementById(modalName + "CloseButton");;

    // When the user clicks on the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
            $('#igv-div').css('display', "none");
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
            $('#igv-div').css('display', "block");
    }

    close.onclick = function() {
        modal.style.display = "none";
            $('#igv-div').css('display', "block");
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
                $('#igv-div').css('display', "block");
        }
    }
}

function fillDaFeatures() {

    modalBody = $('#DAModalBody')

    for (type in DA_FILE) {
        modalName = "DAfeatureFiles" + type

        title = '<br><div></span><h3>' +
            type + '</h3></div><br>'
        modalBody.append(title)

        table = '<table id="' + modalName +'"></table>'
        modalBody.append(table)

        featureFiles = $('#' + modalName)
        header = '<tr><th>Selected</th><th>Cell type</th></tr>'
        featureFiles.append(header)

        for (dafile in DA_FILE[type]) {

            checkbox='<tr><td><input type="checkbox" name ="'+ dafile  +'" value="' +
                'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/significant/'
                + type + "/" + DA_FILE[type][dafile] + '"></td>'

            label='<td><label for="' + dafile + '">' + dafile + '</label><br></td></tr>'
            featureFiles.append(checkbox + label)
        }

    }
}

function fillClusterSpecificFeatures() {
    modalBody = $('#specificModalBody')

    title = '<br><div></span><h3> Cell type specific features </h3></div><br>'
    modalBody.append(title)

    placeholder = '<table id="specificALL"></table>'
    modalBody.append(placeholder)

    featureFiles = $('#specificALL')
    header = '<tr><th>Selected</th><th>Peak list</th></tr>'
    featureFiles.append(header)


    for (type in TRACK_FILE) {
        color = CELLTYPE_DICT[type]["color"]

        checkbox='<tr><td><input type="checkbox" name="'+ type  +'" value="' +
            'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/clusterspecific/' +
            type + '.specific.elements.bed" data-ftype="bed" data-label="Specific peaks ' + type + '" data-sorting="' + type + 'feature"></td>'
        label='<td><label for="' + dafile + '" style="color:' + color + '">' + type + '</label></td></tr>'
        featureFiles.append(checkbox + label)
    }
}


function fillAdditionalFeatures(fileType, headerstr, featureType) {
    modalBody = $('#specificModalBody')
    fileTypeId = fileType.split(" ").join("")

    title = '<br><div></span><h3>' +
        fileType + '</h3></div><br>'
    modalBody.append(title)

    placeholder = '<table id="specific' + fileTypeId + '"></table>'
    modalBody.append(placeholder)

    featureFiles = $('#specific' + fileTypeId)

    header = '<tr><th>Selected</th><th>' + headerstr + '</th></tr>'
    featureFiles.append(header)

    for (addfile in ADDITIONAL_FEATURE_FILES[fileType]) {

        if (addfile in CELLTYPE_DICT) {
            color = CELLTYPE_DICT[addfile]['color']
        } else {
            color = "#656565"
        }

        checkbox='<tr><td><input type="checkbox" name="'+ addfile  +'" value="' +
            'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/' +
            ADDITIONAL_FEATURE_FILES[fileType][addfile] + '" data-ftype="' + ADDITIONAL_FEATURE_FILE_TYPE[fileType]  + '" data-label="' + featureType + " " + addfile + '" data-sorting="' + addfile + 'featureAdd"></td>'
        label='<td><label for="' + addfile + '" style="color:' + color + '">' + addfile + '</label></td></tr>'
        featureFiles.append(checkbox + label)
    }

}

function fillFeatures() {

    fillAdditionalFeatures('All peaks', 'Peak list', 'Peaks')
    fillClusterSpecificFeatures()
    fillAdditionalFeatures('Co-accessible peaks', 'Co-accessible peaks', 'Cicero')
}

function fillTracks() {

    modalBody = $('#trackModalBody')

    title = '<br><h3>All chambers</h3><br>'
    modalBody.append(title)
    table = '<table id="trackALL"></table>'
    modalBody.append(table)

    featureFiles = $('#trackALL')
    header = '<tr><th>Selected</th><th>Cell type</th><th>Scaling group</th></tr>'
    featureFiles.append(header)

    for (type in TRACK_FILE) {

        color = CELLTYPE_DICT[type]["color"]
        inputId = type.split("_").join("")

        sortID = type

        if (type in TRACK_SORTING) {
            sortID = TRACK_SORTING[sortID]
        }

        checkbox ='<tr><td><input type="checkbox" data-group="'+ inputId
            + '" name="'+ type + '" data-sortID="'+ sortID + '" value="' +
            'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/bigwigs/new/' +
            TRACK_FILE[type] + '"></td>'
        label='<td><label for="' + dafile + '" style="color:' + color + '">' + type + '</label></td>'
        groupField = '<td><input type="text" value="allTracks" id="'
            + inputId  + '"></td></tr>'
        featureFiles.append(checkbox + label + groupField)
    }

    for (chamber in CHAMBERS) {
        chamberID = chamber.split(" ").join("")
        title = '<br><h3>' + chamber + '</h3><br>'
        modalBody.append(title)
        table = '<table id="chamber' + chamberID + '"></table>'
        modalBody.append(table)

        featureFiles = $('#chamber' + chamberID)
        header = '<tr><th>Selected</th><th>Cell type</th><th>Scaling group</th></tr>'
        featureFiles.append(header)

        for (type in TRACK_FILE) {
            if (CHAMBERS_FILTER[chamber].includes(type)) {
                continue
            }

            sortID = type

            if (type in TRACK_SORTING) {
                sortID = TRACK_SORTING[sortID]
            }

            color = CELLTYPE_DICT[type]["color"]
            console.log(chamber + " #### " + CHAMBERS[chamber])
            chamberID = chamber.split(" ").join("")
            inputId = chamberID + type.split("_").join("")

            checkbox='<tr><td><input type="checkbox" data-group="'+ inputId
                + '" + name="'+ chamber + " " + type  + '" data-sortID="'+ sortID + '" value="' +
                'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/bigwigs/' +
                CHAMBERS[chamber] + '/' + TRACK_FILE[type] + '"></td>'

            label = '<td><label for="' + dafile + '" style="color:' + color + '">' + type + '</label></td>'
            groupField = '<td><input type="text" value="' + chamberID +
            '" id="' + inputId  + '"></td></tr>'
            featureFiles.append(checkbox + label + groupField)
        }
    }
}


function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function initDefaultTracks() {

    celltype = "Atrial_cardiomyocyte"

    track = {
        "format": 'bigwig',
        "autoscaleGroup": "default",
        "name": celltype,
        "height": 80,
        "color": CELLTYPE_DICT[celltype]["color"],
        "url": "http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/bigwigs/new/" + celltype + ".bw"
    }

    TRACKS.push(track)

    track = {
        "format": 'bed',
        "name": 'Specific peaks ' + celltype.split("_").join(" "),
        "height": 40,
        "color": CELLTYPE_DICT[celltype]["color"],
        "url": 'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/clusterspecific/' +
            celltype + '.specific.elements.bed'
    }

    TRACKS.push(track)

    celltype = "Ventricular_cardiomyocyte"

    track = {
        "format": 'bigwig',
        "autoscaleGroup": "default",
        "name": celltype.split("_").join(" "),
        "height": 80,
        "color": CELLTYPE_DICT[celltype]["color"],
        "url": "http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/bigwigs/new/" + celltype + ".bw"
    }

    TRACKS.push(track)

    track = {
        "format": 'bed',
        "name": 'Specific peaks' + celltype,
        "height": 40,
        "color": CELLTYPE_DICT[celltype]["color"],
        "url": 'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/clusterspecific/' +
            celltype + '.specific.elements.bed'
    }

    TRACKS.push(track)
}


function collectTracksAndLaunchIGV() {
    launchIGV(collectTracks())
}


function collectTracks() {
    tracks = []

    checkboxes = $('#trackSelectionModal input[type=checkbox]:checked')

    for (i=0;i<checkboxes.length;i++){
        checkbox = checkboxes[i]

        name = checkbox.name
        split = name.split(" ")
        type = split[split.length - 1]

        color = CELLTYPE_DICT[type]["color"]
        url = checkbox.value
        groupField = checkbox.dataset.group
        group = $('#' + groupField).val()

        console.log('#### selected track group:' + group + " for name:" + name)

        track = {
            "format": 'bigwig',
            "autoscaleGroup": group,
            "name": name,
            "color": color,
            "url": url,
            "height": 80,
            "sorting": name,
        }

        tracks.push(track)

    }

    checkboxes = $('#specificSelectionModal input[type=checkbox]:checked')

    for (i=0;i<checkboxes.length;i++){
        checkbox = checkboxes[i]

        name = checkbox.name
        split = name.split(" ")

        color = null

        if (name in CELLTYPE_DICT) {
            color = CELLTYPE_DICT[name]["color"]
        }

        url = checkbox.value
        format = checkbox.dataset.ftype
        sorting = checkbox.dataset.sorting
        label = checkbox.dataset.label

        console.log("NAME:" + name + " COLOR:" + color)

        track = {
            "format": format,
            "name": label,
            "color": color,
            "url": url,
            "sorting": sorting
        }

        console.log('#### selected type:' + type)

        tracks.push(track)

    }

    checkboxes = $('#featuresSelectionModal input[type=checkbox]:checked')

    for (i=0;i<checkboxes.length;i++){
        checkbox = checkboxes[i]

        name = checkbox.name
        url = checkbox.value

        track = {
            "format": 'bed',
            "name": name,
            "url": url,
            "height": 30,
            "sorting": name + "Z"
        }

        tracks.push(track)

    }

    tracks = tracks.sort((a, b) => a.sorting > b.sorting ? 1: -1)

    return tracks
}

function launchIGV(tracks) {
    LOCUS = BROWSER.toJSON()['locus'][0]
    $('#igv-div').remove()
    igvDiv = $('#igv-div-style')
    igvDiv.append('<div id="igv-div"></div>')

    var options =
        {
            genome: "hg38",
            autoscaleGroup: true,
            locus: LOCUS,
            tracks: tracks
        };

    var igvDiv = document.getElementById("igv-div");

    igv.createBrowser(igvDiv, options)
        .then(function (browser) {
            BROWSER = browser
            console.log("Created IGV browser");
            formatIGV();
        })
}

function formatIGV() {
    searchCont = $('.igv-search-container')
    searchCont.css('width', '400px')

    textInput = searchCont.find('input')
    textInput.css('font-size', '14px')
    textInput.css('font-weight', 'bold')

    labels = $('.igv-track-label')
    labels.css('font-size', 'large')

    // tracks = $('.igv-track-div')
    // tracks.css('border-style', 'ridge')

}

function downloadSVG(filename) {
    var element = document.createElement('a');
    text = BROWSER.toSVG()

    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}
