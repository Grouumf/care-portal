function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(r, g, b) {
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

CELLTYPE_DICT = {

    // "Fibroblast": {"color": "#fb9600"},
    // "Atrial.Cardiomyocyte": {"color": "#fb7e7a"},
    // "Ventricular.Cardiomyocyte": {"color": "#901102"},

    "Fibroblast": {"color": rgbToHex(251, 150, 0)},
    "Atrial_cardiomyocyte": {"color": rgbToHex(251, 126, 122)},
    "Ventricular_cardiomyocyte": {"color": rgbToHex(144, 17, 2)},
    "Smooth_muscle": {"color": rgbToHex(121, 121, 65)},
    "Endothelial": {"color": rgbToHex(17, 80, 147)},
    "Macrophage": {"color": rgbToHex(146, 13, 255)},
    "Lymphocyte": {"color": rgbToHex(124, 121, 255)},
    "Nervous": {"color": rgbToHex(26, 0, 255)},
    "Adipocyte": {"color": rgbToHex(25, 145, 80)},
}

TRACKS = []

for (const celltype in CELLTYPE_DICT) {
    track = {
        "format": 'bigwig',
        "autoscaleGroup": '1'
    }
    track["name"] = celltype
    track["color"] = CELLTYPE_DICT[celltype]["color"]
    track["url"] = "http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/bigwigs/" + celltype + ".bw"

    TRACKS.push(track)


    track = {
        "format": 'bed',
    }
    track["name"] = "specific features"
    track["color"] = CELLTYPE_DICT[celltype]["color"]
    track["url"] = "http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/clusterspecific/" + celltype + ".specific.elements.bed"

    TRACKS.push(track)
}

TRACKS.push(
        track = {
            "format": 'bed',
	    "name": "MACS2 peaks",
	    "url": 'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/peaks/all.merged.bed'
        }
)

TRACKS.push(
    track = {
        // "type": "bedpe",
            "format": 'bedpe',
	    "name": "cicero links with 0.20 cutoff",
	    "url": 'http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/cicero/cicero.linkages.snATAC.020cutoff.bedpe'
        }
)
