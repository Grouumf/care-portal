
TRACK_FILE = {
    "Adipocyte": "Adipocyte.bw",
    "Atrial_cardiomyocyte": "Atrial_cardiomyocyte.bw",
    "Endothelial": "Endothelial.bw",
    "Fibroblast": "Fibroblast.bw",
    "Lymphocyte": "Lymphocyte.bw",
    "Macrophage": "Macrophage.bw",
    "Nervous": "Nervous.bw",
    "Smooth_muscle": "Smooth_muscle.bw",
    "Ventricular_cardiomyocyte": "Ventricular_cardiomyocyte.bw",
}

TRACK_SORTING = {
    "Adipocyte": 8,
    "Atrial_cardiomyocyte": 3,
    "Endothelial": 4,
    "Fibroblast": 1,
    "Lymphocyte": 7,
    "Macrophage": 6,
    "Nervous": 9,
    "Smooth_muscle": 5,
    "Ventricular_cardiomyocyte": 2,
}


ADDITIONAL_FEATURE_FILES = {
    "Co-accessible peaks": {
        "cicero 0.40 cutoff": "cicero/cicero.linkages.snATAC.040cutoff.bedpe",
        "cicero 0.30 cutoff": "cicero/cicero.linkages.snATAC.030cutoff.bedpe",
        "cicero 0.20 cutoff": "cicero/cicero.linkages.snATAC.020cutoff.bedpe",
        "cicero 0.15 cutoff": "cicero/cicero.linkages.snATAC.015cutoff.bedpe",
        "cicero 0.10 cutoff": "cicero/cicero.linkages.snATAC.015cutoff.bedpe",
        "cicero no cutoff": "cicero/cicero.linkages.snATAC.nocutoff.bedpe",
    },
    "All peaks": {
        "Adipocyte": "features/peaks/merged//Adipocyte.merged.bed",
        "all peaks": "features/peaks/all.merged.bed",
        "Atrial_cardiomyocyte": "features/peaks/merged//Atrial_cardiomyocyte.merged.bed",
        "Endothelial": "features/peaks/merged//Endothelial.merged.bed",
        "Fibroblast": "features/peaks/merged//Fibroblast.merged.bed",
        "Lymphocyte": "features/peaks/merged//Lymphocyte.merged.bed",
        "Macrophage": "features/peaks/merged//Macrophage.merged.bed",
        "Nervous": "features/peaks/merged//Nervous.merged.bed",
        "Smooth_muscle": "features/peaks/merged//Smooth_muscle.merged.bed",
        "Ventricular_cardiomyocyte": "features/peaks/merged//Ventricular_cardiomyocyte.merged.bed",
    }
}

ADDITIONAL_FEATURE_FILE_TYPE = {
    "Co-accessible peaks": "bedpe",
    "All peaks": "bed"
}

LOCUS = "chr14:23,370,000-23,450,000"
var BROWSER = null

CHAMBERS = {
    "Right Atrium": "Chamber_A",
    "Right Ventricle": "Chamber_B",
    "Left Atrium": "Chamber_C",
    "Left Ventricule": "Chamber_D"
}

CHAMBERS_FILTER = {
    "Right Atrium": ["Nervous", "Lymphocyte", "Smooth_muscle", "Ventricular_cardiomyocyte"],
    "Right Ventricle": ["Nervous", "Lymphocyte", "Smooth_muscle", "Atrial_cardiomyocyte"],
    "Left Atrium": ["Nervous", "Lymphocyte", "Smooth_muscle", "Ventricular_cardiomyocyte"],
    "Left Ventricule": ["Nervous", "Lymphocyte", "Smooth_muscle", "Atrial_cardiomyocyte"]
}



DA_FILE = {
    "atria_vs_ventricle": {
        "Cardiomyocyte Atria vs Ventricle.Atrial_DA": "Cardiomyocyte.DA.test.AtriavsVentricle.Atrial_DA.txt",
        "Cardiomyocyte Atria vs Ventricle.Ventricular_DA": "Cardiomyocyte.DA.test.AtriavsVentricle.Ventricular_DA.txt",
        "Endothelial Atria vs Ventricle.Ventricular_DA": "Endothelial.DA.test.AtriavsVentricle.Ventricular_DA.txt",
        "Fibroblast Atria vs Ventricle.Atrial_DA": "Fibroblast.DA.test.AtriavsVentricle.Atrial_DA.txt",
        "Fibroblast Atria vs Ventricle.Ventricular_DA": "Fibroblast.DA.test.AtriavsVentricle.Ventricular_DA.txt",
        "SmoothMuscle Atria vs Ventricle.Atrial_DA": "SmoothMuscle.DA.test.AtriavsVentricle.Atrial_DA.txt",
        "SmoothMuscle Atria vs Ventricle.Ventricular_DA": "SmoothMuscle.DA.test.AtriavsVentricle.Ventricular_DA.txt",
    },
    "LA_vs_LV" : {
        "Cardiomyocyte LA vs LV.LA_DA": "Cardiomyocyte.DA.test.LAvsLV.LA_DA.txt",
        "Cardiomyocyte LA vs LV.LV_DA": "Cardiomyocyte.DA.test.LAvsLV.LV_DA.txt",
        "Endothelial LA vs LV.LA_DA": "Endothelial.DA.test.LAvsLV.LA_DA.txt",
        "Fibroblast LA vs LV.LA_DA": "Fibroblast.DA.test.LAvsLV.LA_DA.txt",
        "Fibroblast LA vs LV.LV_DA": "Fibroblast.DA.test.LAvsLV.LV_DA.txt",
        "Macrophage LA vs LV.LA_DA": "Macrophage.DA.test.LAvsLV.LA_DA.txt",
    },
    "RA_vs_LA" : {
        "Cardiomyocyte RA vs LA.LA_DA": "Cardiomyocyte.DA.test.RAvsLA.LA_DA.txt",
        "Cardiomyocyte RA vs LA.RA_DA": "Cardiomyocyte.DA.test.RAvsLA.RA_DA.txt",
        "Fibroblast RA vs LA.LA_DA": "Fibroblast.DA.test.RAvsLA.LA_DA.txt",
        "Fibroblast RA vs LA.RA_DA": "Fibroblast.DA.test.RAvsLA.RA_DA.txt",
        "Macrophage RA vs LA.LA_DA": "Macrophage.DA.test.RAvsLA.LA_DA.txt",
        "SmoothMuscle RA vs LA.LA_DA": "SmoothMuscle.DA.test.RAvsLA.LA_DA.txt",
        "SmoothMuscle RA vs LA.RA_DA": "SmoothMuscle.DA.test.RAvsLA.RA_DA.txt",
    },
    "RA_vs_RV": {
        "Cardiomyocyte RA vs RV.RA_DA": "Cardiomyocyte.DA.test.RAvsRV.RA_DA.txt",
        "Cardiomyocyte RA vs RV.RV_DA": "Cardiomyocyte.DA.test.RAvsRV.RV_DA.txt",
        "Endothelial RA vs RV.RA_DA": "Endothelial.DA.test.RAvsRV.RA_DA.txt",
        "Fibroblast RA vs RV.RA_DA": "Fibroblast.DA.test.RAvsRV.RA_DA.txt",
        "Fibroblast RA vs RV.RV_DA": "Fibroblast.DA.test.RAvsRV.RV_DA.txt",
        "Macrophage RA vs RV.RV_DA": "Macrophage.DA.test.RAvsRV.RV_DA.txt",
        "SmoothMuscle RA vs RV.RA_DA": "SmoothMuscle.DA.test.RAvsRV.RA_DA.txt",
        "SmoothMuscle RA vs RV.RV_DA": "SmoothMuscle.DA.test.RAvsRV.RV_DA.txt",
    },
    "RV_vs_LV": {
        "Cardiomyocyte RV vs LV.LV_DA": "Cardiomyocyte.DA.test.RVvsLV.LV_DA.txt",
        "Cardiomyocyte RV vs LV.RV_DA": "Cardiomyocyte.DA.test.RVvsLV.RV_DA.txt",
        "Endothelial RV vs LV.RV_DA": "Endothelial.DA.test.RVvsLV.RV_DA.txt",
        "Fibroblast RV vs LV.LV_DA": "Fibroblast.DA.test.RVvsLV.LV_DA.txt",
        "Fibroblast RV vs LV.RV_DA": "Fibroblast.DA.test.RVvsLV.RV_DA.txt",
        "Macrophage RV vs LV.RV_DA": "Macrophage.DA.test.RVvsLV.RV_DA.txt",
    }
}

CELLTYPE_DICT = {

    "Fibroblast": {"color": rgbToHex(251, 150, 0)},
    "Atrial_cardiomyocyte": {"color": rgbToHex(251, 126, 122)},
    "Ventricular_cardiomyocyte": {"color": rgbToHex(144, 17, 2)},
    "Smooth_muscle": {"color": rgbToHex(121, 121, 65)},
    "Endothelial": {"color": rgbToHex(17, 80, 147)},
    "Macrophage": {"color": rgbToHex(146, 13, 255)},
    "Lymphocyte": {"color": rgbToHex(124, 121, 255)},
    "Nervous": {"color": rgbToHex(26, 0, 255)},
    "Adipocyte": {"color": rgbToHex(25, 145, 80)},
}

TRACKS = []
