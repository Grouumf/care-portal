.. _DISEASE_GENETIC:

Disease genetics
=================

This section provides data related to the genetic diseases eQTL and GWAS analyis.


Atrial Fibrillation and Coronary Artery Disease Fine mapping results
--------------------------------------------------------------------

These files are the fine mapped 99% credible sets of GWAS variants for Atrial Fibrillation (1) and Coronary Artery Disease (2) summary statistics. To construct credible sets of variants for each locus, we first extracted all variants in linkage disequilibrium (r > 0.1 using the EUR subset of 1000 Genomes Phase 3) in a large window (±2.5 Mb) around each index variant. We next calculated approximate Bayes factors (ABF) for each variant using effect size and standard error estimates. We then calculated posterior probabilities of association (PPA) for each variant by dividing its ABF by the sum of ABF for all variants within the locus. For each locus, we then defined 99% credible sets by sorting variants by descending PPA and retaining variants that added up to a cumulative PPA of > 0.99. Genome coordinates are provided in hg19 and hg38.
(1) Nielsen, J. B. et al. Biobank-driven genomic discovery yields new insight into atrial fibrillation biology. Nat Genet 50, 1234-1239, doi:10.1038/s41588-018-0171-3 (2018).
(2) Nelson, C. P. et al. Association analyses based on false discovery rate implicate new loci for coronary artery disease. Nature Genetics 49, 1385-1391, doi:10.1038/ng.3913 (2017).

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `99credset.AtrialFibrillation.tsv <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/gwas/99credset.AtrialFibrillation.tsv>`_
     - 655K
     - 03/12/20
     - Atrial Fibrillation GWAS variants fine mapped
   * - `99credset.CoronaryArteryDisease.tsv <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/gwas/99credset.CoronaryArteryDisease.tsv>`_
     - 482K
     - 03/12/20
     - Coronary Artery Disease GWAS variants fine mapped


AF variant-cardiomyocyte element screen results
-----------------------------------------------


Fine mapped AF variants with PPA > 0.10 were intersected with peaks called on aggregate cardiomyocyte snATAC data. Genomic coordinates are in hg38. Highlighted enhancer-variant pairs were selected for enhancer validation (two fine mapped AF variants reside within the candidate enhancer at the KCNJ5 locus). Annotations for candidate enhancer-variant pairs were generated using HOMER with the following command:

.. code-block:: shell

   annotatePeaks.pl hg38 {peak file} > {output file}

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `AtrialFibrillation.variant.screen.result.xlsx <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/gwas/AtrialFibrillation.variant.screen.result.xlsx>`_
     - 20K
     - 03/12/20
     - Fine mapped AF variants
