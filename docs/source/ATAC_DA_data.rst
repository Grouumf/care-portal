.. _DA_analysis:

single nucleus ATAC-seq Chamber to Chamber comparison data
===========================================================

These data files contains edgeR results for for each cell type when performing Chamber to chamber comparison. These results include both significant and non signifcant features. Significant DA features can be obtained by filtering by positive or negative fold change (for atrial vs. ventricular enriched) and bu using an FDR < 0.05 cutoff.  There are four chambers considered: Left and right atria, and left and right ventricules. We also performed significant features between left and right chambers. All the files can be downloaded from the `HTTP server <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential>`_ and from the tables below. The abbreviations are LA: Left Atrium, LV: Left Ventricule, RA: Right Atrium, and RV Right Ventricule. Also, the significant features can be downloaded from `http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/significant/ <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/significant/>`_

Cardiomyocytes
--------------

Cardiomyocytes Specific features

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `Cardiomyocyte.DA.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Cardiomyocyte.DA.test.AtriavsVentricle.txt>`_
     - 12M
     - 03/25/20
     - Cardiomyocyte Atria vs Ventricle significant features
   * - `Cardiomyocyte.DA.test.LAvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Cardiomyocyte.DA.test.LAvsLV.txt>`_
     - 15M
     - 03/25/20
     - Cardiomyocyte LA vs LV significant features
   * - `Cardiomyocyte.DA.test.RAvsLA.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Cardiomyocyte.DA.test.RAvsLA.txt>`_
     - 12M
     - 03/25/20
     - Cardiomyocyte RA vs LA significant features
   * - `Cardiomyocyte.DA.test.RAvsRV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Cardiomyocyte.DA.test.RAvsRV.txt>`_
     - 12M
     - 03/25/20
     - Cardiomyocyte RA vs RV significant features
   * - `Cardiomyocyte.DA.test.RightvsLeft.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Cardiomyocyte.DA.test.RightvsLeft.txt>`_
     - 12M
     - 03/25/20
     - Cardiomyocyte Right vs Left significant features
   * - `Cardiomyocyte.DA.test.RVvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Cardiomyocyte.DA.test.RVvsLV.txt>`_
     - 12M
     - 03/25/20
     - Cardiomyocyte RV vs LV significant features

Endothelial
--------------

Endothelial Specific features

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `Endothelial.DA.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Endothelial.DA.test.AtriavsVentricle.txt>`_
     - 402K
     - 03/25/20
     - Endothelial Atria vs Ventricle significant features
   * - `Endothelial.DA.test.LAvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Endothelial.DA.test.LAvsLV.txt>`_
     - 1.1M
     - 03/25/20
     - Endothelial LA vs LV significant features
   * - `Endothelial.DA.test.RAvsLA.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Endothelial.DA.test.RAvsLA.txt>`_
     - 66K
     - 03/25/20
     - Endothelial RA vs LA significant features
   * - `Endothelial.DA.test.RAvsRV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Endothelial.DA.test.RAvsRV.txt>`_
     - 466K
     - 03/25/20
     - Endothelial RA vs RV significant features
   * - `Endothelial.DA.test.RightvsLeft.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Endothelial.DA.test.RightvsLeft.txt>`_
     - 352K
     - 03/25/20
     - Endothelial Right vs Left significant features
   * - `Endothelial.DA.test.RVvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Endothelial.DA.test.RVvsLV.txt>`_
     - 383K
     - 03/25/20
     - Endothelial RV vs LV significant features

Fibroblast
--------------

Fibroblast Specific features

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `Fibroblast.DA.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Fibroblast.DA.test.AtriavsVentricle.txt>`_
     - 5.7M
     - 03/25/20
     - Fibroblast Atria vs Ventricle significant features
   * - `Fibroblast.DA.test.LAvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Fibroblast.DA.test.LAvsLV.txt>`_
     - 5.4M
     - 03/25/20
     - Fibroblast LA vs LV significant features
   * - `Fibroblast.DA.test.RAvsLA.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Fibroblast.DA.test.RAvsLA.txt>`_
     - 4.7M
     - 03/25/20
     - Fibroblast RA vs LA significant features
   * - `Fibroblast.DA.test.RAvsRV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Fibroblast.DA.test.RAvsRV.txt>`_
     - 6.2M
     - 03/25/20
     - Fibroblast RA vs RV significant features
   * - `Fibroblast.DA.test.RightvsLeft.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Fibroblast.DA.test.RightvsLeft.txt>`_
     - 5.4M
     - 03/25/20
     - Fibroblast Right vs Left significant features
   * - `Fibroblast.DA.test.RVvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Fibroblast.DA.test.RVvsLV.txt>`_
     - 5.6M
     - 03/25/20
     - Fibroblast RV vs LV significant features

Macrophage
--------------
Macrophage Specific features

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `Macrophage.DA.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Macrophage.DA.test.AtriavsVentricle.txt>`_
     - 948K
     - 03/25/20
     - Macrophage Atria vs Ventricle significant features
   * - `Macrophage.DA.test.LAvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Macrophage.DA.test.LAvsLV.txt>`_
     - 1.2M
     - 03/25/20
     - Macrophage LA vs LV significant features
   * - `Macrophage.DA.test.RAvsLA.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Macrophage.DA.test.RAvsLA.txt>`_
     - 390K
     - 03/25/20
     - Macrophage RA vs LA significant features
   * - `Macrophage.DA.test.RAvsRV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Macrophage.DA.test.RAvsRV.txt>`_
     - 1.0M
     - 03/25/20
     - Macrophage RA vs RV significant features
   * - `Macrophage.DA.test.RightvsLeft.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Macrophage.DA.test.RightvsLeft.txt>`_
     - 858K
     - 03/25/20
     - Macrophage Right vs Left significant features
   * - `Macrophage.DA.test.RVvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/Macrophage.DA.test.RVvsLV.txt>`_
     - 1.1M
     - 03/25/20
     - Macrophage RV vs LV significant features

Smooth Muscle
--------------

Smooth Muscle Specific features

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `SmoothMuscle.DA.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/SmoothMuscle.DA.test.AtriavsVentricle.txt>`_
     - 1.3M
     - 03/25/20
     - SmoothMuscle Atria vs Ventricle significant features
   * - `SmoothMuscle.DA.test.LAvsLV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/SmoothMuscle.DA.test.LAvsLV.txt>`_
     - 1.6M
     - 03/25/20
     - SmoothMuscle LA vs LV significant features
   * - `SmoothMuscle.DA.test.RAvsLA.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/SmoothMuscle.DA.test.RAvsLA.txt>`_
     - 957K
     - 03/25/20
     - SmoothMuscle RA vs LA significant features
   * - `SmoothMuscle.DA.test.RAvsRV.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/SmoothMuscle.DA.test.RAvsRV.txt>`_
     - 1.4M
     - 03/25/20
     - SmoothMuscle RA vs RV significant features
   * - `SmoothMuscle.DA.test.RightvsLeft <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/features/differential/SmoothMuscle.DA.test.RightvsLeft>`_
     - 1.2M
     - 03/25/20
     - SmoothMuscle Right vs Left significant features


single nucleus ATAC-seq Chamber to Chamber comparison data
===========================================================

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `Cardiomyocyte.DE.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/Cardiomyocyte.DE.test.AtriavsVentricle.txt>`_
     - 825K
     - 03/25/20
     - CM Atria vs Ventricle Differential Genes
   * - `Fibroblast.DE.test.AtriavsVentricle.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/Fibroblast.DE.test.AtriavsVentricle.tx>`_
     - 493K
     - 03/25/20
     - FB Atria vs Ventricle Differential Genes
