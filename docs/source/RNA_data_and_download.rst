single-cell RNA datasets
========================
Available CARE snRNA-Seq data. This section reports most of the data related to RNA seq data analysis. lternatively, all the snRNA data be downloaded from the `HTTP server <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/>`_.


Realease updates
----------------

* Initial release for the CARE study (03/25/2020)



Bigwig tracks
--------------

.. list-table:: Data files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - RNA-Seq reads bigwig file
   * - `Adipocyte.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Adipocyte.snRNA.bw>`_
     - 11M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Aggregate.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Aggregate.snRNA.bw>`_
     - 497M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Arterialar.Smooth_muscle.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Arterialar.Smooth_muscle.snRNA.bw>`_
     - 9.8M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Atrial_cardiomyocyte.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Atrial_cardiomyocyte.snRNA.bw>`_
     - 132M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Endocardial.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Endocardial.snRNA.bw>`_
     - 11M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Endothelial.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Endothelial.snRNA.bw>`_
     - 72M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Fibroblast.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Fibroblast.snRNA.bw>`_
     - 112M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Lymphocyte.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Lymphocyte.snRNA.bw>`_
     - 7.3M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Macrophage.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Macrophage.snRNA.bw>`_
     - 34M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Myofibroblast.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Myofibroblast.snRNA.bw>`_
     - 6.4M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Pericyte.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Pericyte.snRNA.bw>`_
     - 38M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Nervous.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Nervous.snRNA.bw>`_
     - 4.8M
     - 03/24/20
     - RNA-Seq reads bigwig file
   * - `Ventricular_cardiomyocyte.snRNA.bw <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bigwigs/Ventricular_cardiomyocyte.snRNA.bw>`_
     - 266M
     - 03/24/20
     - RNA-Seq reads bigwig file


Bed files
---------

These bed files contain the information linking the cell's ID  and the reads.

.. list-table:: Bed files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `all.bed.gz <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/bedfiles/all.bed.gz>`_
     - 8.7G
     - 03/24/20
     - Description


Matrices
--------

Different matrix formats are accessible for download. *snRNA_Heart_data_matrix.mtx* and *snRNA_Heart_count_matrix.mtx* are raw count and UMI matrices that goes along the barcode and features indexes *snRNA_Heart_barcodes.tsv* and *snRNA_Heart_features.tsv*. *exprMatrix.tsv.gz* is a more convenient matrix written as a tsv file with both indexes included:

.. code-block:: shell

   zcat exprMatrix.tsv.gz|cut -f1,2,3,4,5,6|head
   NF2_LA_AAACCCAAGACTAGAT NF2_LA_AAACCCAAGATAACGT NF2_LA_AAACCCAAGCATGTTC NF2_LA_AAACCCAAGCCTTCTC NF2_LA_AAACCCACAAAGCGTG
   MIR1302-2HG     0.0     0.0     0.0     0.0     0.0
   AL627309.1      0.0     0.0     0.0     0.0     0.0
   AC114498.1      0.0     0.0     0.0     0.0     0.0
   AL669831.5      0.0     0.0     0.0     0.0     0.0
   FAM87B  0.0     0.0     0.0     0.0     0.0
   LINC00115       0.0     0.0     0.0     0.0     0.0
   FAM41C  0.0     0.0     0.0     0.0     0.0
   AL645608.7      0.0     0.0     0.0     0.0     0.0
   AL645608.1      0.0     0.0     0.0     0.0     0.0


.. list-table:: Bed files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `snRNA_Heart_barcodes.tsv <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/matrix/snRNA_Heart_barcodes.tsv>`_
     - 810K
     - 03/26/20
     - single-cell barcodes
   * - `snRNA_Heart_counts_matrix.mtx <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/matrix/snRNA_Heart_counts_matrix.mtx>`_
     - 460M
     - 03/26/20
     - single cell count matrix in mtx format
   * - `snRNA_Heart_data_matrix.mtx <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/matrix/snRNA_Heart_data_matrix.mtx>`_
     - 1.1G
     - 03/26/20
     - single cell matrix in mtx format
   * - `snRNA_Heart_features.tsv <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/matrix/snRNA_Heart_features.tsv>`_
     - 345K
     - 03/26/20
     - feature index in tsv format
   * - `exprMatrix.tsv.gz <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/matrix/exprMatrix.tsv.gz>`_
     - 520M
     - 03/30/20
     - matrix in tsv format including x and y indexes


Metadata
--------

barcode metadata

.. list-table:: Bed files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `snRNA_Heart_metadata.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/metadata/snRNA_Heart_metadata.txt>`_
     - 16M
     - 03/26/20
     - barcode metadata


Consensus
---------

.. list-table:: Bed files
   :widths: 10 5 10 40

   * - File
     - Size
     - Last modified
     - Description
   * - `Normalized.gene.expression.by.cluster.snRNA.txt <http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/features/consensus/Normalized.gene.expression.by.cluster.snRNA.txt>`_
     - 6.1M
     - 03/24/20
     - Consensus table for snRNA/snATAC integration
