snATAC-Seq visualisation
========================

UMAP projection
---------------

UMAP projection of the 79,515 ATAC cells and the 9 clusters

.. image:: _static/CARE_Portal_snATAC_1.png


single-cell genome browser
~~~~~~~~~~~~~~~~~~~~~~~~~~

An interactive UCSC singlec-cell browser session can be found here: `http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/ucsc_browser/ds=snATAC <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/ucsc_browser/\?ds=snATAC>`_ which visualises the opened and signficant promoters per cell-type.

Also, an interactive UCSC singlec-cell browser session can be found here: `http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/ucsc_browser/ds=snATACMotif <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/ucsc_browser/\?ds=snATACMotif>`_ which visualises the opened and signficant promoters per cell-type.


Genome browser tracks
---------------------

Cell subtype browser track
~~~~~~~~~~~~~~~~~~~~~~~~~~~

A genome browser track is available at this address: http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/IGV/IGV_bigwig.html


Chamber browser track
~~~~~~~~~~~~~~~~~~~~~

A genome browser track is available at this address: http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/IGV/IGV_chamber_bigwig.html


snRNA-Seq visualisation
========================

UMAP projection
---------------

UMAP projection of the 12 clusters

.. image:: _static/CARE_Portal_snRNA-cropped.png


single-cell genome browser
~~~~~~~~~~~~~~~~~~~~~~~~~~

An interactive UCSC singlec-cell browser session can be found here: `http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/ucsc_browser/ds=snRNA <http://ns104190.ip-147-135-44.us/data_CARE_portal/snATAC/ucsc_browser/\?ds=snRNA>`_


Genome browser tracks
---------------------

A genome browser track is available at this address: http://ns104190.ip-147-135-44.us/data_CARE_portal/snRNA/IGV/IGV_bigwig.html
