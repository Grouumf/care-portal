Manipulating snATAC sparse matrices
===================================

COO format uses integer indexes for both columns (features) and rows (cells). In this first section, we describe how to load them in Python or R.

Loading COO matrix in R
~~~~~~~~~~~~~~~~~~~~~~~

In R, two packages ar required: `Matrix` and `data.table`.

.. code-block:: R

   library("Matrix")
   library("data.table")

   # ygi file path for peak index
   # xgi file path for barcode index
   # mfile file path for matrix file in coo

   peak_ids = fread(
    ygi,
    sep="\t", header = FALSE)

   cell_ids = fread(
    xgi,
    sep="\t", header = FALSE)

   dat = fread(mfile, sep="\t")

   mat = sparseMatrix(i = dat$V1 + 1, j =dat$V2 + 1, x=dat$V3)
